#!/bin/bash
rm ./dist -r

echo "Compilando proyecto ..."
npm run build:aws

echo "Genernando Imagen Docker"
docker build -t aeroman-ng-datahub:1.0.4 .

echo "Exportando Imagen Docker"
docker save aeroman-ng-datahub:1.0.4 > aeroman-ng-datahub.tar
scp aeroman-ng-datahub.tar -i /home/rex2002xp/Develop/orasoft/keys/sif.pem ec2-user@ec2-3-16-94-191.us-east-2.compute.amazonaws.com:/home/ec2-user/tmp/aeroman-ng-datahub.tar

echo "Eliminar TAR"
rm aeroman-ng-datahub.tar

echo "..................."
echo ".  Deploy Remote" .
echo "..................."

ssh -i /home/rex2002xp/Develop/orasoft/keys/sif.pem ec2-user@ec2-3-16-94-191.us-east-2.compute.amazonaws.com 'bash -s ' < deploy_remote.sh
