import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { P404Component, P500Component } from './components';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '404',
    component: P404Component,
    data: {
      title: 'Page 404'
    }
  },
  {
    path: '500',
    component: P500Component,
    data: {
      title: 'Page 500'
    }
  },
];

@NgModule({
  declarations: [
    P404Component,
    P500Component
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class ErrorModule {
}
