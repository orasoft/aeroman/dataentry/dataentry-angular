export interface ITokenInfo {
  access_token: string;
  token_type: string;
  refresh_token: string;
  expires_in: number;
  scope: string;
  full_name: string;
  sub_ptr_id: number;
  sub_ptr_name: string;
  inst_id: number;
  inst_name: string;
  user: number;
  jti: string;
}
