import { Component } from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})
export class LoginComponent {

  public username: string;
  public password: string;

  constructor( private authService: AuthService,
               private _notification: ToastrService,
               private router: Router) {}

  public login() {
    this.authService.login(this.username, this.password).subscribe( data => {
      if (data > 0) {
        this.router.navigate(['dashboard']);
      } else {
        this._notification.error('Authentication Error.', 'Login');
      }
    }, error => {
      this._notification.error('Authentication Error.', 'Connection Fail');
    });
    //
    //
    //
    // if ( this.username === 'SuperUser' && this.password === 'aeroman'
    //   // this.username !== undefined &&
    //   // this.username !== null &&
    //   // this.username !== '' &&
    //   // this.password !== undefined &&
    //   // this.password !== null &&
    //   // this.password !== ''
    // ) {
    //   this.router.navigate(['dashboard']);
    //   // this.authService.obtainAccessToken(this.username, this.password).subscribe( resp => {
    //   //   if (resp.access_token) {
    //   //     localStorage.setItem('token', resp.access_token);
    //   //     this.router.navigate(['dashboard']);
    //   //   }
    //   // });
    //
    // }
  }


}
