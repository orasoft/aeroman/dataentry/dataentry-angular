import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {finalize} from 'rxjs/operators';

import {environment} from '../../../environments/environment';


@Injectable()
export class AuthService {
  private _url = environment.apiUrl;

  constructor(private readonly _http: HttpClient) {
  }

  public login(username, password): Observable<Number> {
    // const params = new URLSearchParams()
    // params.append('username', username)
    // params.append('password', password)
    // params.append('grant_type', 'password')
    // params.append('client_id', 'feDataHub')
    // const headers = new HttpHeaders({
    //   'Content-type': 'application/x-www-form-urlencoded; charset=utf-8',
    //   'Authorization': 'Basic ' + btoa('feDataHub:aeroman')
    // })
    return this._http.post<Number>(`${this._url}/auth`,  { username, password });
  }

}
