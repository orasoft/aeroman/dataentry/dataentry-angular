import { Component } from '@angular/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'base-template',
  template: `<router-outlet></router-outlet>`
})
export class BaseLayoutComponent {}
