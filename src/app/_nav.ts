interface NavAttributes {
  [propName: string]: any;
}

interface NavWrapper {
  attributes: NavAttributes;
  element: string;
}

interface NavBadge {
  text: string;
  variant: string;
}

interface NavLabel {
  class?: string;
  variant: string;
}

export interface NavData {
  name?: string;
  url?: string;
  icon?: string;
  badge?: NavBadge;
  title?: boolean;
  children?: NavData[];
  variant?: string;
  attributes?: NavAttributes;
  divider?: boolean;
  class?: string;
  label?: NavLabel;
  wrapper?: NavWrapper;
}

export const navItems: NavData[] = [
  {
    name: 'Home',
    url: '/dashboard',
    icon: 'fa fa-home',
  },
  {
    title: true,
    name: 'DATAENTRY'
  },
  {
    name: 'Critical Paths',
    url: '/dataentry/critical-path/resume',
    icon: 'fa fa-map',
  },
  {
    name: 'Daily Focus',
    url: '/dataentry/daily-focus/resume',
    icon: 'fa fa-map',
  },
  // {
  //   title: true,
  //   name: 'DATAHUB',
  // },
  // {
  //   name: 'Mapping Resumen',
  //   url: '/datahub/mapping-config/resume',
  //   icon: 'fa fa-map',
  // }
];
