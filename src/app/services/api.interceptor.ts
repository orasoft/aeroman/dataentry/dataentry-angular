import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable()
export class ApiInterceptor implements HttpInterceptor {

  private cloned: HttpRequest<any>;
  private urlRequest: string;

  intercept(req: HttpRequest<any>,
            next: HttpHandler): Observable<HttpEvent<any>> {

    this.urlRequest = req.url;
    const idToken = localStorage.getItem('token');
    const url = '/oauth/token';

    this.cloned = req.clone();

    if (idToken) {
      if (req.url.search(url) === -1) {
        this.cloned = req.clone({
          headers: req.headers.set('Authorization',
            'Bearer ' + idToken)
        });
      }
    }
    return next.handle(this.cloned);
  }

}
