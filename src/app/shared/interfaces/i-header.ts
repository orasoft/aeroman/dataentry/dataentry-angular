export interface IHeader {
  code: number;
  description: string;
}
