import { IResponseGeneric } from '../interfaces/i-response-generic';
import { IHeader } from '../interfaces/i-header';

export abstract class ResponseGenericData<T> implements IResponseGeneric {
  data: T;
  header: IHeader;
}
