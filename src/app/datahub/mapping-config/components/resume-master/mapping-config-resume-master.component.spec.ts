import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MappingConfigResumeMasterComponent } from './mapping-config-resume-master.component';

describe('ResumeComponent', () => {
  let component: MappingConfigResumeMasterComponent;
  let fixture: ComponentFixture<MappingConfigResumeMasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MappingConfigResumeMasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MappingConfigResumeMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
