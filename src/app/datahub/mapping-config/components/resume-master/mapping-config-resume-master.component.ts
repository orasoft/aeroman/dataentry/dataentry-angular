import {Component, OnInit, ViewChild} from '@angular/core';
import {API, APIDefinition, Columns, Config, DefaultConfig} from 'ngx-easy-table';
import {CatalogDataHubService} from '../../../shared/service/catalog-datahub.service';
import {IModel} from '../../../shared/interfaces/i-model';
import {IMappingConfig} from '../../interfaces/i-mapping-config';
import {MappingConfigService} from '../../services/mapping-config.service';
import {ToastrService} from 'ngx-toastr';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';

@Component({
  selector: 'app-mapping-config-resume-master',
  templateUrl: './mapping-config-resume-master.component.html',
  styleUrls: ['./mapping-config-resume-master.component.scss']
})
export class MappingConfigResumeMasterComponent implements OnInit {

  public TITLE = 'Mapping Configuration';
  @ViewChild('table', {static: false}) table: APIDefinition;

  public toggledRows = new Set<number>();

  /* Parent */
  public columns: Columns[] = [
    {key: 'modName', title: 'MODELS', width: '80%'},
    {key: '', title: 'SHOW / HIDE', width: '20%'},
  ];
  public parentData: IModel[] = [];
  public parentConfiguration: Config;

  /* Nested */
  public nestedConfiguration;
  public nestedColumns: Columns[] = [
    {key: 'comName', title: 'COMPANY', width: '20%'},
    {key: 'tableOutName', title: 'TABLE OUT'},
    {key: 'fieldOutName', title: 'FIELD OUT'},
    {key: 'tableInName', title: 'TABLE IN'},
    {key: 'fieldInName', title: 'FIELD IN'}
  ];
  public nestedData: IMappingConfig[][] = [[]];

  public companyId: number;
  public modelId: number;
  public modalRef: BsModalRef;
  public typeRule: string;

  constructor(private readonly _modelService: CatalogDataHubService,
              private readonly _mappingConfigService: MappingConfigService,
              private readonly _toastrService: ToastrService,
              private _modalService: BsModalService) {
    this.companyId = 1;
    this.modelId = 2;
  }

  ngOnInit(): void {
    this.getModels();
  }

  public getModels() {
    this._modelService.getModels().subscribe(response => {
      this.parentData = response.data.filter((element) => element.modId !== 1);
      this.setConfigurationTableParent();
    });
  }

  public getAllRecords(modelId: number) {
    this._mappingConfigService.getItemsByModelsAndCompany(this.modelId, this.companyId).subscribe(response => {
      if (response.header.code === 200) {
        this.nestedData[modelId] = response.data;
        this._toastrService.success('Action Complete', this.TITLE);
        if (this.nestedData.length === 0) {
          this._toastrService.info('Record not found', this.TITLE);
        }
      } else {
        this._toastrService.error('Could not load data.', this.TITLE);
      }
    }, error => {
      this._toastrService.error('Could not load data.', this.TITLE);
    });
  }

  public setCompanyId(id: number) {
    this.companyId = id;
    this.nestedData = [[]];
    this.setConfigurationTableParent();
  }

  eventEmitted($event: { event: string, value: { event: any, row: IMappingConfig } }, template): void {
    this.modalRef = this._modalService.show(template, {
      class: 'modal-lg',
      backdrop: true,
      keyboard: false
    });
  }

  public onRowClickEvent($event: MouseEvent, index: number): void {
    $event.preventDefault();
    this.table.apiEvent({
      type: API.toggleRowIndex,
      value: index
    });
    this.modelId = this.parentData[index].modId;
    if (this.toggledRows.has(index)) {
      this.toggledRows.delete(index);
    } else {
      this.toggledRows.add(index);
      this.getAllRecords(this.modelId);
    }
  }

  public getConfiguration(row) {
    const config = {...DefaultConfig};
    config.detailsTemplate = true;
    config.paginationEnabled = false;
    config.tableLayout.striped = false;
    config.selectRow = true;
    config.clickEvent = true;
    config.rows = 1000000;
    return config;
  }

  private setConfigurationTableParent() {
    this.parentConfiguration = {...DefaultConfig};
    this.parentConfiguration.detailsTemplate = true;
    this.parentConfiguration.tableLayout.hover = false;
    this.parentConfiguration.paginationEnabled = false;
  }
}
