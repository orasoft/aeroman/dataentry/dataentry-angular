import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IByPassElement } from '../../interfaces/i-by-pass-element';

@Component({
  selector: 'app-mapping-config-by-pass-fields-cart',
  templateUrl: './mapping-config-by-pass-fields-cart.component.html',
  styleUrls: ['./mapping-config-by-pass-fields-cart.component.scss']
})
export class MappingConfigByPassFieldsCartComponent implements OnInit {

  @Input() elements: IByPassElement[];
  @Output() deleteElementEmit = new EventEmitter<IByPassElement>();

  constructor() {
    this.elements = [];
  }

  ngOnInit() {
  }

  deleteEmit(position: IByPassElement) {
    this.deleteElementEmit.emit(position);
  }

}
