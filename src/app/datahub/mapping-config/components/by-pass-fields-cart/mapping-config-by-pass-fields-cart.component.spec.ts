import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MappingConfigByPassFieldsCartComponent } from './mapping-config-by-pass-fields-cart.component';

describe('ByPassFieldsCartComponent', () => {
  let component: MappingConfigByPassFieldsCartComponent;
  let fixture: ComponentFixture<MappingConfigByPassFieldsCartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MappingConfigByPassFieldsCartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MappingConfigByPassFieldsCartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
