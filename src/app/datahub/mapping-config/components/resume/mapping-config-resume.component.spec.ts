import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MappingConfigResumeComponent } from './mapping-config-resume.component';

describe('ResumeComponent', () => {
  let component: MappingConfigResumeComponent;
  let fixture: ComponentFixture<MappingConfigResumeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MappingConfigResumeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MappingConfigResumeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
