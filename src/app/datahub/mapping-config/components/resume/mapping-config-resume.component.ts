// import {Component, OnInit} from '@angular/core';
// import {Columns, Config, DefaultConfig} from 'ngx-easy-table';
// import {IMappingConfig} from '../../interfaces/i-mapping-config';
// import {MappingConfigService} from '../../services/mapping-config.service';
// import {BsModalService} from 'ngx-bootstrap';
// import {NgxSpinnerService} from 'ngx-spinner';
// import {ToastrService} from 'ngx-toastr';
// import {Router} from '@angular/router';
//
// @Component({
//   selector: 'app-mapping-config-resume',
//   templateUrl: './mapping-config-resume.component.html',
//   styleUrls: ['./mapping-config-resume.component.scss']
// })
// export class MappingConfigResumeComponent implements OnInit {
//
//   public TITLE = 'Mapping Configuration';
//   public configuration: Config;
//   public columns: Columns[];
//   public records: Array<IMappingConfig>;
//
//   public toggleRowIndex;
//   public groupBy = 'modelName';
//   public clicked: string;
//
//   constructor(private _modalService: BsModalService,
//               private _service: MappingConfigService,
//               private spinner: NgxSpinnerService,
//               private _toastrService: ToastrService,
//               private _routes: Router) {
//     this.records = [];
//   }
//
//   ngOnInit(): void {
//     this.getAllRecords();
//     this.configuration = {...DefaultConfig};
//     this.configuration.tableLayout.striped = true;
//     this.configuration.groupRows = true;
//     this.configuration.paginationRangeEnabled = false;
//     this.configuration.fixedColumnWidth = true;
//     this.configuration.selectRow = true;
//     this.columns = [
//       {key: '', title: 'MODELS', width: '20%'},
//       {key: 'tableOutName', title: 'TABLE OUT'},
//       {key: 'fieldOutName', title: 'FIELD OUT'},
//       {key: 'tableInName', title: 'TABLE IN'},
//       {key: 'fieldInName', title: 'FIELD IN'},
//       {key: '', title: 'ACTION'}
//     ];
//   }
//
//   public getAllRecords() {
//     this.spinner.show().then(() => {
//       this._service.getList().subscribe(response => {
//         this.spinnerHide().then(() => {
//           if (response.header.code === 200) {
//             this.records = response.data;
//             this._toastrService.success('Action Complete', this.TITLE);
//             if (this.records.length === 0) {
//               this._toastrService.info('Record not found', this.TITLE);
//             }
//           } else {
//             this._toastrService.error('Could not load data.', this.TITLE);
//           }
//         });
//       }, error => {
//         this.spinnerHide().then(() => {
//           this._toastrService.error('Could not load data.', this.TITLE);
//         });
//       });
//     });
//   }
//
//   spinnerHide() {
//     return new Promise(resolve => {
//       setTimeout(() => {
//         this.spinner.hide().then(() => {
//           resolve();
//         });
//       }, 250);
//     });
//   }
//
//   onRowClickEvent($event: MouseEvent, index: number): void {
//     $event.preventDefault();
//     this.toggleRowIndex = {index};
//     console.log(`this.toggleRowIndex => ${JSON.stringify($event)}`);
//   }
//
//   eventEmitted($event: { event: string, value: any }): void {
//     this.clicked = JSON.stringify($event, null, 2);
//     // tslint:disable-next-line:no-console
//     console.log('$event', $event);
//   }
//
//   onChange(groupBy: string): void {
//     this.groupBy = groupBy;
//   }
// }
