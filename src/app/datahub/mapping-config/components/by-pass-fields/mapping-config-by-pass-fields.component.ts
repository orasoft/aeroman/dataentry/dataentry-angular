import { Component, OnInit } from '@angular/core';
import { CatalogDatahubService } from '../../../../dataentry/shared/services/catalog-datahub.service';
import { ICompany } from '../../../shared/interfaces/i-company';
import { ITableIn } from '../../../shared/interfaces/i-table-in';
import { IFieldsIn } from '../../../shared/interfaces/i-fields-in';
import { IModel } from '../../../shared/interfaces/i-model';
import { ITableOut } from '../../../shared/interfaces/i-table-out';
import { IFieldsOut } from '../../../shared/interfaces/i-fields-out';
import { Router } from '@angular/router';
import { MappingConfigService } from '../../services/mapping-config.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { IByPassElement } from '../../interfaces/i-by-pass-element';

@Component({
  selector: 'app-by-pass-fields',
  templateUrl: './mapping-config-by-pass-fields.component.html',
  styleUrls: ['./mapping-config-by-pass-fields.component.scss']
})
export class MappingConfigByPassFieldsComponent implements OnInit {

  public TITLE = 'Mapping Config - By Pass';
  public formByPass: FormGroup;
  public byPassElements: IByPassElement[];
  public companySelected: ICompany;
  public tableInSelected: ITableIn;
  public fieldInSelected: IFieldsIn;
  public modelSelected: IModel;
  public tableOutSelected: ITableOut;
  public fieldOutSelected: IFieldsOut;

  constructor(private _catalogDatahubService: CatalogDatahubService,
              private _mappingConfigService: MappingConfigService,
              private _notification: ToastrService,
              private _fb: FormBuilder,
              private spinner: NgxSpinnerService,
              private router: Router) {
    this.byPassElements = [];
    this.companySelected = {comId: 0, comName: '', aDateCreate: '', aDateUpdate: '', aUserCreate: '', aUserUpdate: ''};
    this.tableInSelected = {tableInId: 0, tableInName: '', comId: 0, tableInDescription: '', tableInSource: ''};
    this.modelSelected = {modId: 0, modName: '', aDateCreate: '', aDateUpdate: '', aUserCreate: '', aUserUpdate: ''};
    this.tableOutSelected = {tableOutId: 0, modId: 0, tableOutName: '', tableOutDescription: ''};
  }

  ngOnInit() {
    this.formInit();
  }

  spinnerHide() {
    return new Promise(resolve => {
      setTimeout(() => {
        this.spinner.hide().then(() => {
          resolve();
        });
      }, 250);
    });
  }

  save() {
    this.spinner.show().then(() => {
      const queue = [];
      let count = 1;
      this.byPassElements.forEach(element => {
        queue.push(this.saveElement(element, count));
        count++;
      });

      const task = Promise.all(queue);


      task.then(() => {
        this.spinnerHide().then(() => {
          this.gotoResume();
        });
      });

    });
  }

  formInit() {
    this.formByPass = this._fb.group({
      fieldOutId: [0, Validators.compose([Validators.required, Validators.min(1)])],
      fieldInId: [0, Validators.compose([Validators.required, Validators.min(1)])],
    });
  }

  saveElement(element: IByPassElement, count: number) {
    return new Promise((resolve) => {
      setTimeout(() => {
        this._mappingConfigService.setCreateRecord(element.fieldOutId, element.fieldInId)
          .subscribe(data => {
            this._notification.success('Record saved.', 'Mapping Config - ByPass');
            resolve();
          }, error => {
            this._notification.error('It was not possible to save the record.', 'Mapping Config - ByPass');
            resolve();
          });
      }, 500 * count);
    });
  }

  cancel() {
    this.gotoResume();
  }

  gotoResume() {
    return this.router.navigate(['/datahub/mapping-config/resume']);
  }

  addItem() {
    const itemByPass: IByPassElement = {
      companyId: this.companySelected.comId,
      companyName: this.companySelected.comName,
      tableInId: this.tableInSelected.tableInId,
      tableInName: this.tableInSelected.tableInName,
      fieldInId: this.fieldInSelected.fieldInId,
      fieldInName: this.fieldInSelected.fieldInName,
      modelId: this.modelSelected.modId,
      modelName: this.modelSelected.modName,
      tableOutId: this.tableOutSelected.tableOutId,
      tableOutName: this.tableOutSelected.tableOutName,
      fieldOutId: this.fieldOutSelected.fieldOutId,
      fieldOutName: this.fieldOutSelected.fieldOutName
    };

    const elementExist = this.byPassElements.filter(
      (item) => (item.fieldOutId === itemByPass.fieldOutId) && item.fieldInId === itemByPass.fieldInId
    );

    if (elementExist.length === 0) {
      this.byPassElements.push(itemByPass);
    }
  }

  deleteElementByPass(element: IByPassElement) {
    this.byPassElements = [...this.byPassElements.filter((a) => a !== element)];
  }

  changeEventCompany(company: ICompany) {
    console.log(`Recibido => ${JSON.stringify(company)}`);
    this.companySelected = company;
  }

  changeEventTableIn(tableIn: ITableIn) {
    console.log(`Recibido => ${JSON.stringify(tableIn)}`);
    this.tableInSelected = tableIn;
  }

  changeEventFieldIn(fieldIn: IFieldsIn) {
    console.log(`Recibido => ${JSON.stringify(fieldIn)}`);
    this.fieldInSelected = fieldIn;
  }

  changeEventModel(model: IModel) {
    console.log(`Recibido => ${JSON.stringify(model)}`);
    this.modelSelected = model;
  }

  changeEventTableOut(tableOut: ITableOut) {
    console.log(`Recibido => ${JSON.stringify(tableOut)}`);
    this.tableOutSelected = tableOut;
  }

  changeEventFieldOut(fieldOut: IFieldsOut) {
    console.log(`Recibido => ${JSON.stringify(fieldOut)}`);
    this.fieldOutSelected = fieldOut;
  }

}
