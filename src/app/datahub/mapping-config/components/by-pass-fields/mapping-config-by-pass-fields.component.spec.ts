import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MappingConfigByPassFieldsComponent } from './mapping-config-by-pass-fields.component';

describe('ByPassFieldsComponent', () => {
  let component: MappingConfigByPassFieldsComponent;
  let fixture: ComponentFixture<MappingConfigByPassFieldsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MappingConfigByPassFieldsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MappingConfigByPassFieldsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
