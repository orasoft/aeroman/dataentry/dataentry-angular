import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { ResponseMappingConfigResumeData } from '../messages/response-mapping-config-resume-data';


@Injectable()
export class MappingConfigService {
  private _url = environment.apiUrl + '/datahub';

  constructor(private readonly _http: HttpClient) {
  }

  public getList() {
    return this._http.get<ResponseMappingConfigResumeData>(`${this._url}/mapping/config`);
  }

  public getItemsByModelsAndCompany(modelId: number, companyId: number) {
    return this._http.get<ResponseMappingConfigResumeData>(`${this._url}/mapping/config/model/${modelId}/company/${companyId}`);
  }

  public setCreateRecord(fieldOutId: number, fieldInId: number) {
    const message = {
      fieldOutId,
      fieldInId
    };
    return this._http.post(`${this._url}/mapping/config`, message);
  }
}
