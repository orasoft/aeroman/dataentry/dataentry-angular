import { ResponseGenericData } from '../../../shared/responses/response-generic-data';
import { IMappingConfig } from '../interfaces/i-mapping-config';
import { IHeader } from '../../../shared/interfaces/i-header';


export class ResponseMappingConfigResumeData implements ResponseGenericData<IMappingConfig[]> {
  data: IMappingConfig[];
  header: IHeader;
}
