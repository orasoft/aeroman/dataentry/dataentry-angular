export interface IMappingConfig {
  modId: number;
  modelName: string;
  comId: number;
  comName: string;
  tableOutId: number;
  tableOutName: string;
  fieldOutId: number;
  fieldOutName: string;
  tableInId: number;
  tableInName: string;
  fieldInId: number;
  fieldInName: string;
  typeId: number;
  typeName: string;
  ruleValueSet: string;
}
