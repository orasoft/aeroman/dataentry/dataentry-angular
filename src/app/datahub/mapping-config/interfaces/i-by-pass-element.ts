export interface IByPassElement {
  companyId: number;
  companyName: string;
  tableInId: number;
  tableInName: string;
  fieldInId: number;
  fieldInName: string;
  modelId: number;
  modelName: string;
  tableOutId: number;
  tableOutName: string;
  fieldOutId: number;
  fieldOutName: string;
}
