import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {NgxPaginationModule} from 'ngx-pagination';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MappingConfigService} from './services/mapping-config.service';
import {MappingConfigByPassFieldsComponent} from './components/by-pass-fields/mapping-config-by-pass-fields.component';
import {SharedDataentryModule} from '../../dataentry/shared/shared-dataentry.module';
import {MappingConfigByPassFieldsCartComponent} from './components/by-pass-fields-cart/mapping-config-by-pass-fields-cart.component';
import {TableModule} from 'ngx-easy-table';
import {SharedDataHubModule} from '../shared/shared-datahub.module';
import {MappingConfigResumeMasterComponent} from './components/resume-master/mapping-config-resume-master.component';
import {ModalModule} from 'ngx-bootstrap';

const routes: Routes = [
  {
    path: 'resume',
    component: MappingConfigResumeMasterComponent,
    data: {
      title: 'Mapping Configuration'
    }
  },
  {
    path: 'bypass',
    component: MappingConfigByPassFieldsComponent,
    data: {
      title: 'Mapping Configuration'
    }
  }
];

@NgModule({
  declarations: [
    MappingConfigByPassFieldsComponent,
    MappingConfigByPassFieldsCartComponent,
    MappingConfigResumeMasterComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    NgxPaginationModule,
    FormsModule,
    ReactiveFormsModule,
    SharedDataentryModule,
    SharedDataHubModule,
    TableModule,
    ModalModule
  ],
  providers: [
    MappingConfigService
  ]
})
export class MappingConfigModule {
}
