import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { Observable } from 'rxjs';
import { ResponseCompanyData } from '../response/response-company-data';
import { ResponseTableInData } from '../response/response-table-in-data';
import { ResponseFieldsInData } from '../response/response-fields-in-data';
import { ResponseModelsData } from '../response/response-models-data';
import { ResponseTableOutData } from '../response/response-table-out-data';
import { ResponseFieldsOutData } from '../response/response-fields-out-data';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CatalogDataHubService {

  private _url = environment.apiUrl + '/datahub';

  constructor(private readonly _http: HttpClient) { }

  getCompanies(): Observable<ResponseCompanyData> {
    return this._http.get<ResponseCompanyData>(`${this._url}/company`);
  }

  getTableIn(companyId: number): Observable<ResponseTableInData> {
    return this._http.get<ResponseTableInData>(`${this._url}/table_in?companyId=${companyId}`);
  }

  getFieldsIn(tableInId: number): Observable<ResponseFieldsInData> {
    return this._http.get<ResponseFieldsInData>(`${this._url}/fields_in?tableInId=${tableInId}`);
  }

  getModels(): Observable<ResponseModelsData> {
    return this._http.get<ResponseModelsData>(`${this._url}/models`);
  }

  getTablesOut(modelId: number): Observable<ResponseTableOutData> {
    return this._http.get<ResponseTableOutData>(`${this._url}/table_out?modelId=${modelId}`);
  }

  getFieldsOut(tableOutId: number): Observable<ResponseFieldsOutData> {
    return this._http.get<ResponseFieldsOutData>(`${this._url}/fields_out?tableOutId=${tableOutId}`);
  }
}
