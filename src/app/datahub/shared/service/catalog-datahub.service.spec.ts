import { TestBed } from '@angular/core/testing';

import { CatalogDataHubService } from './catalog-datahub.service';

describe('CatalogDataHubService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CatalogDataHubService = TestBed.get(CatalogDataHubService);
    expect(service).toBeTruthy();
  });
});
