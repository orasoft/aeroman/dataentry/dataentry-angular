import { ResponseGenericData } from '../../../shared/responses/response-generic-data';
import { IHeader } from '../../../shared/interfaces/i-header';
import { ITableOut } from '../interfaces/i-table-out';


export class ResponseTableOutData implements ResponseGenericData<ITableOut[]> {
  data: ITableOut[];
  header: IHeader;
}
