import { ResponseGenericData } from '../../../shared/responses/response-generic-data';
import { IHeader } from '../../../shared/interfaces/i-header';
import { ITableIn } from '../interfaces/i-table-in';


export class ResponseTableInData implements ResponseGenericData<ITableIn[]> {
  data: ITableIn[];
  header: IHeader;
}
