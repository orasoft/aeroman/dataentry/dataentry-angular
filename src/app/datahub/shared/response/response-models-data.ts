import { ResponseGenericData } from '../../../shared/responses/response-generic-data';
import { IHeader } from '../../../shared/interfaces/i-header';
import { ITableIn } from '../interfaces/i-table-in';
import { IModel } from '../interfaces/i-model';


export class ResponseModelsData implements ResponseGenericData<IModel[]> {
  data: IModel[];
  header: IHeader;
}
