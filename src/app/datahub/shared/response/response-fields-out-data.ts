import { ResponseGenericData } from '../../../shared/responses/response-generic-data';
import { IHeader } from '../../../shared/interfaces/i-header';
import { IFieldsOut } from '../interfaces/i-fields-out';


export class ResponseFieldsOutData implements ResponseGenericData<IFieldsOut[]> {
  data: IFieldsOut[];
  header: IHeader;
}
