import { ICompany } from '../interfaces/i-company';
import { ResponseGenericData } from '../../../shared/responses/response-generic-data';
import { IHeader } from '../../../shared/interfaces/i-header';


export class ResponseCompanyData implements ResponseGenericData<ICompany[]> {
  data: ICompany[];
  header: IHeader;
}
