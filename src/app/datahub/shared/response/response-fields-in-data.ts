import { ResponseGenericData } from '../../../shared/responses/response-generic-data';
import { IHeader } from '../../../shared/interfaces/i-header';
import { IFieldsIn } from '../interfaces/i-fields-in';


export class ResponseFieldsInData implements ResponseGenericData<IFieldsIn[]> {
  data: IFieldsIn[];
  header: IHeader;
}
