import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CatalogDataHubService } from './service/catalog-datahub.service';
import { CatalogDataHubCompanyComponent } from './components/catalog-data-hub-company/catalog-data-hub-company.component';
import { FormsModule } from '@angular/forms';
import { CatalogDataHubTableInComponent } from './components/catalog-data-hub-table-in/catalog-data-hub-table-in.component';
import { CatalogDataHubFieldInComponent } from './components/catalog-data-hub-field-in/catalog-data-hub-field-in.component';
import { CatalogDataHubModelComponent } from './components/catalog-data-hub-model/catalog-data-hub-model.component';
import { CatalogDataHubTableOutComponent } from './components/catalog-data-hub-table-out/catalog-data-hub-table-out.component';
import { CatalogDataHubFieldOutComponent } from './components/catalog-data-hub-field-out/catalog-data-hub-field-out.component';

@NgModule({
  declarations: [
    CatalogDataHubCompanyComponent,
    CatalogDataHubTableInComponent,
    CatalogDataHubFieldInComponent,
    CatalogDataHubModelComponent,
    CatalogDataHubTableOutComponent,
    CatalogDataHubFieldOutComponent
  ],
  imports: [
    CommonModule,
    FormsModule
  ],
  providers: [
    CatalogDataHubService
  ],
  exports: [
    CatalogDataHubCompanyComponent,
    CatalogDataHubTableInComponent,
    CatalogDataHubFieldInComponent,
    CatalogDataHubModelComponent,
    CatalogDataHubTableOutComponent,
    CatalogDataHubFieldOutComponent
  ]
})
export class SharedDataHubModule { }
