import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CatalogDataHubFieldInComponent } from './catalog-data-hub-field-in.component';

describe('CatalogDataHubCompanyComponent', () => {
  let component: CatalogDataHubFieldInComponent;
  let fixture: ComponentFixture<CatalogDataHubFieldInComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CatalogDataHubFieldInComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CatalogDataHubFieldInComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
