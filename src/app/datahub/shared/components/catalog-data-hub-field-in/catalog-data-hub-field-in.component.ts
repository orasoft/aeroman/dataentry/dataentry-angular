import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { CatalogDataHubService } from '../../service/catalog-datahub.service';
import { IFieldsIn } from '../../interfaces/i-fields-in';

@Component({
  selector: 'app-catalog-data-hub-field-in',
  templateUrl: './catalog-data-hub-field-in.component.html',
  styleUrls: ['./catalog-data-hub-field-in.component.scss']
})
export class CatalogDataHubFieldInComponent implements OnInit, OnChanges {

  @Output() notification = new EventEmitter<IFieldsIn>();
  @Input() fieldInSelectedQuery?: number;
  @Input() tableInSelectedId: number;

  public records: IFieldsIn[];
  public recordId: number;

  constructor(private _service: CatalogDataHubService) {
    this.records = [];
    this.recordId = 0;
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log(`changes => ${JSON.stringify(changes)}`);
    for (const propName in changes) {
      const change = changes[propName];
      if (propName === 'tableInSelectedId' && change.currentValue > 0) {
        this.getFieldsIn();
      }

      if (propName === 'tableInSelectedId' && change.currentValue === 0) {
        this.records = [];
        this.recordId = 0;
      }

      if (propName === 'fieldInSelectedQuery' && change.currentValue > 0 && this.records.length > 0) {
        if (this.records.find(item => item.fieldInId === change.currentValue)) {
          this.recordId = this.records.filter(item => item.fieldInId === change.currentValue)[0].fieldInId;
        } else {
          this.recordId = this.records[0].fieldInId;
        }
        this.notificationElementSelected();
      }
    }
  }

  public notificationElementSelected() {
    const a = this.records.filter((item) => Number(item.fieldInId) === Number(this.recordId))[0];
    this.notification.emit(a);
  }

  private getFieldsIn() {
    this._service.getFieldsIn(this.tableInSelectedId).subscribe(message => {
      this.records = message.data;
      this.recordId = this.records[0].fieldInId;
      this.notificationElementSelected();
    });
  }

}
