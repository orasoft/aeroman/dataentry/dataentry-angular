import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CatalogDataHubTableInComponent } from './catalog-data-hub-table-in.component';

describe('CatalogDataHubCompanyComponent', () => {
  let component: CatalogDataHubTableInComponent;
  let fixture: ComponentFixture<CatalogDataHubTableInComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CatalogDataHubTableInComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CatalogDataHubTableInComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
