import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { CatalogDataHubService } from '../../service/catalog-datahub.service';
import { ITableIn } from '../../interfaces/i-table-in';

@Component({
  selector: 'app-catalog-data-hub-table-in',
  templateUrl: './catalog-data-hub-table-in.component.html',
  styleUrls: ['./catalog-data-hub-table-in.component.scss']
})
export class CatalogDataHubTableInComponent implements OnInit, OnChanges {

  @Output() notification = new EventEmitter<ITableIn>();
  @Input() tableInSelectedQuery?: number;
  @Input() companySelectedId: number;

  public records: ITableIn[];
  public recordId: number;

  constructor(private _service: CatalogDataHubService) {
    this.records = [];
    this.recordId = 0;
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log(`changes => ${JSON.stringify(changes)}`);
    for (const propName in changes) {
      const change = changes[propName];
      if (propName === 'companySelectedId' && change.currentValue > 0) {
        this.getTablesIn();
      }

      if (propName === 'tableInSelectedQuery' && change.currentValue > 0 && this.records.length > 0) {
        if (this.records.find(item => item.tableInId === change.currentValue)) {
          this.recordId = this.records.filter(item => item.tableInId === change.currentValue)[0].comId;
        } else {
          this.recordId = this.records[0].tableInId;
        }
        this.notificationElementSelected();
      }
    }
  }

  public notificationElementSelected() {
    if (this.recordId > 0) {
      const a = this.records.filter((item) => Number(item.tableInId) === Number(this.recordId))[0];
      this.notification.emit(a);
    } else {
      this.notification.emit({tableInId: 0, tableInName: '', tableInSource: '', tableInDescription: '', comId: 0});
    }
  }

  private getTablesIn() {
    this._service.getTableIn(this.companySelectedId).subscribe(message => {
      this.records = message.data;
      this.recordId = this.records.length > 0 ? this.records[0].tableInId : 0;
      this.notificationElementSelected();
    });
  }

}
