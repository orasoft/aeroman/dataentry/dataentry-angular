import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CatalogDataHubModelComponent } from './catalog-data-hub-model.component';

describe('CatalogDataHubCompanyComponent', () => {
  let component: CatalogDataHubModelComponent;
  let fixture: ComponentFixture<CatalogDataHubModelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CatalogDataHubModelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CatalogDataHubModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
