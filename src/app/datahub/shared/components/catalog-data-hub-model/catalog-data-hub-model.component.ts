import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { CatalogDataHubService } from '../../service/catalog-datahub.service';
import { IModel } from '../../interfaces/i-model';

@Component({
  selector: 'app-catalog-data-hub-model',
  templateUrl: './catalog-data-hub-model.component.html',
  styleUrls: ['./catalog-data-hub-model.component.scss']
})
export class CatalogDataHubModelComponent implements OnInit, OnChanges {

  @Output() notification = new EventEmitter<IModel>();
  @Input() modelSelectedQuery?: number;

  public records: IModel[];
  public recordId: number;

  constructor(private _service: CatalogDataHubService) {
    this.records = [];
  }

  ngOnInit() {
    this.getModels();
  }

  ngOnChanges(changes: SimpleChanges) {
    for (const propName in changes) {
      const change = changes[propName];
      if (propName === 'modelSelectedQuery' && change.currentValue > 0 && this.records.length > 0) {
        if (this.records.find(item => item.modId === change.currentValue)) {
          this.recordId = this.records.filter(item => item.modId === change.currentValue)[0].modId;
        } else {
          this.recordId = this.records[0].modId;
        }
        this.notificationElementSelected();
      }
    }
  }

  public notificationElementSelected() {
    const a = this.records.filter((item) => Number(item.modId) === Number(this.recordId))[0];
    this.notification.emit(a);
  }

  private getModels() {
    this._service.getModels().subscribe(message => {
      this.records = message.data.filter((element) => element.modId !== 1);
    });
  }

}
