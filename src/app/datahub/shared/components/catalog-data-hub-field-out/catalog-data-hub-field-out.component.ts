import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { CatalogDataHubService } from '../../service/catalog-datahub.service';
import { IFieldsOut } from '../../interfaces/i-fields-out';

@Component({
  selector: 'app-catalog-data-hub-field-out',
  templateUrl: './catalog-data-hub-field-out.component.html',
  styleUrls: ['./catalog-data-hub-field-out.component.scss']
})
export class CatalogDataHubFieldOutComponent implements OnInit, OnChanges {

  @Output() notification = new EventEmitter<IFieldsOut>();
  @Input() fieldOutSelectedQuery?: number;
  @Input() tableOutSelectedId: number;

  public records: IFieldsOut[];
  public recordId: number;

  constructor(private _service: CatalogDataHubService) {
    this.records = [];
    this.recordId = 0;
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log(`changes => ${JSON.stringify(changes)}`);
    for (const propName in changes) {
      const change = changes[propName];
      if (propName === 'tableOutSelectedId' && change.currentValue > 0) {
        this.getFieldsOut();
      }

      if (propName === 'tableOutSelectedId' && change.currentValue === 0) {
        this.records = [];
        this.recordId = 0;
      }

      if (propName === 'fieldOutSelectedQuery' && change.currentValue > 0 && this.records.length > 0) {
        if (this.records.find(item => item.fieldOutId === change.currentValue)) {
          this.recordId = this.records.filter(item => item.fieldOutId === change.currentValue)[0].fieldOutId;
        } else {
          this.recordId = this.records[0].fieldOutId;
        }
        this.notificationElementSelected();
      }
    }
  }

  public notificationElementSelected() {
    const a = this.records.filter((item) => Number(item.fieldOutId) === Number(this.recordId))[0];
    this.notification.emit(a);
  }

  private getFieldsOut() {
    this._service.getFieldsOut(this.tableOutSelectedId).subscribe(message => {
      this.records = message.data;
      this.recordId = this.records[0].fieldOutId;
      this.notificationElementSelected();
    });
  }

}
