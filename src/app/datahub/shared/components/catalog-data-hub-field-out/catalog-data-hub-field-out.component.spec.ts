import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CatalogDataHubFieldOutComponent } from './catalog-data-hub-field-out.component';

describe('CatalogDataHubCompanyComponent', () => {
  let component: CatalogDataHubFieldOutComponent;
  let fixture: ComponentFixture<CatalogDataHubFieldOutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CatalogDataHubFieldOutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CatalogDataHubFieldOutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
