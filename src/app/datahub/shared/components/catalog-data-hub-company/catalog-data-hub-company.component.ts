import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { CatalogDataHubService } from '../../service/catalog-datahub.service';
import { ICompany } from '../../interfaces/i-company';

@Component({
  selector: 'app-catalog-data-hub-company',
  templateUrl: './catalog-data-hub-company.component.html',
  styleUrls: ['./catalog-data-hub-company.component.scss']
})
export class CatalogDataHubCompanyComponent implements OnInit, OnChanges {

  @Output() notification = new EventEmitter<ICompany>();
  @Input() companySelectedQuery?: number;

  public records: ICompany[];
  public recordId: number;

  constructor(private _service: CatalogDataHubService) {
    this.records = [];
  }

  ngOnInit() {
    this.getCompanies();
  }

  ngOnChanges(changes: SimpleChanges) {
    for (const propName in changes) {
      const change = changes[propName];
      if (propName === 'companySelectedQuery' && change.currentValue > 0 && this.records.length > 0) {
        if (this.records.find(item => item.comId === change.currentValue)) {
          this.recordId = this.records.filter(item => item.comId === change.currentValue)[0].comId;
        } else {
          this.recordId = this.records[0].comId;
        }
        this.notificationElementSelected();
      }
    }
  }

  public notificationElementSelected() {
    const a = this.records.filter((item) => Number(item.comId) === Number(this.recordId))[0];
    this.notification.emit(a);
  }

  private getCompanies() {
    this._service.getCompanies().subscribe(message => {
      this.records = message.data;
    });
  }

}
