import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CatalogDataHubCompanyComponent } from './catalog-data-hub-company.component';

describe('CatalogDataHubCompanyComponent', () => {
  let component: CatalogDataHubCompanyComponent;
  let fixture: ComponentFixture<CatalogDataHubCompanyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CatalogDataHubCompanyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CatalogDataHubCompanyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
