import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CatalogDataHubTableOutComponent } from './catalog-data-hub-table-out.component';

describe('CatalogDataHubCompanyComponent', () => {
  let component: CatalogDataHubTableOutComponent;
  let fixture: ComponentFixture<CatalogDataHubTableOutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CatalogDataHubTableOutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CatalogDataHubTableOutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
