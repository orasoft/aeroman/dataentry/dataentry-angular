import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { CatalogDataHubService } from '../../service/catalog-datahub.service';
import { ITableOut } from '../../interfaces/i-table-out';

@Component({
  selector: 'app-catalog-data-hub-table-out',
  templateUrl: './catalog-data-hub-table-out.component.html',
  styleUrls: ['./catalog-data-hub-table-out.component.scss']
})
export class CatalogDataHubTableOutComponent implements OnInit, OnChanges {

  @Output() notification = new EventEmitter<ITableOut>();
  @Input() tableOutSelectedQuery?: number;
  @Input() modelSelectedId: number;

  public records: ITableOut[];
  public recordId: number;

  constructor(private _service: CatalogDataHubService) {
    this.records = [];
    this.recordId = 0;
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log(`changes => ${JSON.stringify(changes)}`);
    for (const propName in changes) {
      const change = changes[propName];
      if (propName === 'modelSelectedId' && change.currentValue > 0) {
        this.getTablesOut();
      }

      if (propName === 'tableOutSelectedQuery' && change.currentValue > 0 && this.records.length > 0) {
        if (this.records.find(item => item.tableOutId === change.currentValue)) {
          this.recordId = this.records.filter(item => item.tableOutId === change.currentValue)[0].tableOutId;
        } else {
          this.recordId = this.records[0].tableOutId;
        }
        this.notificationElementSelected();
      }
    }
  }

  public notificationElementSelected() {
    if (this.recordId > 0) {
      const a = this.records.filter((item) => Number(item.tableOutId) === Number(this.recordId))[0];
      this.notification.emit(a);
    } else {
      this.notification.emit({ tableOutId: 0, tableOutName: '', tableOutDescription: '', modId: 0});
    }
  }

  private getTablesOut() {
    this._service.getTablesOut(this.modelSelectedId).subscribe(message => {
      this.records = message.data;
      this.recordId = this.records.length > 0 ? this.records[0].tableOutId : 0;
      this.notificationElementSelected();
    });
  }

}
