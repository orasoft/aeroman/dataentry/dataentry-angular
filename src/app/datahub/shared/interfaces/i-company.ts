export interface ICompany {
  comId: number;
  comName: string;
  aDateCreate: string;
  aUserCreate: string;
  aDateUpdate: string;
  aUserUpdate: string;
}
