export interface IFieldsIn {
  fieldInId: number;
  fieldInName: string;
  fieldInDescription: string;
  tableInId: number;
}
