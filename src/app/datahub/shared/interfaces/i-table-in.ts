export interface ITableIn {
  tableInId: number;
  tableInName: string;
  tableInDescription: string;
  tableInSource: string;
  comId: number;
}
