export interface ITableOut {
  tableOutId: number;
  tableOutName: string;
  tableOutDescription: string;
  modId: number;
}
