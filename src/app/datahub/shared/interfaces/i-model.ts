export interface IModel {
  modId: number;
  modName: string;
  aDateCreate: string;
  aUserCreate: string;
  aDateUpdate: string;
  aUserUpdate: string;
}
