export interface IFieldsOut {
  fieldOutId: number;
  fieldOutName: string;
  fieldOutDescription: string;
  tableOutId: number;
}
