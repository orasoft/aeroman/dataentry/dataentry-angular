import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RulesResumenComponent } from './components/rules-resumen/rules-resumen.component';
import { RouterModule, Routes } from '@angular/router';
import { RulesNewComponent } from './components/rules-new/rules-new.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { FormsModule } from '@angular/forms';
import { RuleService } from './services/rule.service';

const routes: Routes = [
  {
    path: 'resumen',
    component: RulesResumenComponent,
    data: {
      title: 'Rules'
    }
  },
  {
    path: 'new',
    component: RulesNewComponent,
    data: {
      title: 'New Rule'
    }
  }
];

@NgModule({
  declarations: [
    RulesResumenComponent,
    RulesNewComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    NgxPaginationModule,
    FormsModule
  ],
  providers: [
    RuleService
  ]
})
export class RulesModule {
}
