import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RulesNewComponent } from './rules-new.component';

describe('RulesNewComponent', () => {
  let component: RulesNewComponent;
  let fixture: ComponentFixture<RulesNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RulesNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RulesNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
