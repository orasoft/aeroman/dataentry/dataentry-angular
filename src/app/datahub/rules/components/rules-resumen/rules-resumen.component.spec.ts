import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RulesResumenComponent } from './rules-resumen.component';

describe('RulesResumenComponent', () => {
  let component: RulesResumenComponent;
  let fixture: ComponentFixture<RulesResumenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RulesResumenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RulesResumenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
