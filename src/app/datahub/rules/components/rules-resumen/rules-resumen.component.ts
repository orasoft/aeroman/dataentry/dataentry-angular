import { Component, OnInit } from '@angular/core';
import { PaginationInstance } from 'ngx-pagination';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { IRuleResume } from '../../interfaces/i-rule-resume';
import { RuleService } from '../../services/rule.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-rules-resumen',
  templateUrl: './rules-resumen.component.html',
  styleUrls: ['./rules-resumen.component.scss']
})
export class RulesResumenComponent implements OnInit {

  public TITLE = 'Rules';
  public configPage: PaginationInstance = {
    id: 'advanced',
    itemsPerPage: 10,
    currentPage: 1
  };
  public records: IRuleResume[];
  public recordDelete: number;
  public modalRef: BsModalRef;

  constructor(private _modalService: BsModalService,
              private _service: RuleService,
              private _toastrService: ToastrService, private _routes: Router) {
    this.records = [];
  }

  ngOnInit() {
    this.getAllRecords();
  }

  public getAllRecords() {
    this._service.getList().subscribe(response => {
      if (response.header.code === 200) {
        this.records = response.data;
        if (this.records.length === 0) {
          this._toastrService.success('No se encontraron registros', this.TITLE);
        }
      }
    });
  }

  public gotoAddRule() {
    this._routes.navigate(['/datahub/rules/new']);
  }

  public onPageChange(number: number) {
    this.configPage.currentPage = number;
  }

  public confirmation(id, template) {
    this.recordDelete = id;
    this.modalRef = this._modalService.show(template, {
      class: 'modal-primary',
      backdrop: true,
      ignoreBackdropClick: true,
      keyboard: false
    });
  }

  public delete() {
  }

  public closeModal() {
  }

}
