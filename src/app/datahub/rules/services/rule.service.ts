import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { ResponseRuleResumeData } from '../messages/response-rule-resume-data';

@Injectable()
export class RuleService {
  private _url = environment.apiUrl + '/datahub';

  constructor(private readonly _http: HttpClient) {
  }

  public getList() {
    return this._http.get<ResponseRuleResumeData>(`${this._url}/rules`);
  }
}
