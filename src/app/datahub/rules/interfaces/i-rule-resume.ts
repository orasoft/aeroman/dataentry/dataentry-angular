export interface IRuleResume {
  ruleId: number;
  typeRuleHuman: string;
  tableInSource: string;
  tableInName: string;
  fieldInName: string;
  castName: string;
  status: string;
}
