import { ResponseGenericData } from '../../../shared/responses/response-generic-data';
import { IHeader } from '../../../shared/interfaces/i-header';
import { IRuleResume } from '../interfaces/i-rule-resume';

export class ResponseRuleResumeData implements ResponseGenericData<IRuleResume[]> {
  data: IRuleResume[];
  header: IHeader;
}
