import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ResponseDailyFocusData } from '../messages/response-daily-focus-data';
import { environment } from '../../../../environments/environment';


@Injectable()
export class DailyFocusService {

  private _url = environment.apiUrl + '/dataentry';

  constructor(private readonly _http: HttpClient) {
  }

  public listAll(): Observable<ResponseDailyFocusData> {
    return this._http.get<ResponseDailyFocusData>(`${this._url}/daily_focus`);
  }

  public create(body): Observable<ResponseDailyFocusData> {
    return this._http.post<ResponseDailyFocusData>(`${this._url}/daily_focus`, body);
  }

  public update(id: string, body): Observable<ResponseDailyFocusData> {
    return this._http.put<ResponseDailyFocusData>(`${this._url}/daily_focus/${id}`, body);
  }

  public delete(id): Observable<ResponseDailyFocusData> {
    return this._http.delete<ResponseDailyFocusData>(`${this._url}/daily_focus/${id}`);
  }

  public find(value: string): Observable<ResponseDailyFocusData> {
    return this._http.get<ResponseDailyFocusData>(`${this._url}/daily_focus/find/${value}`);
  }
}
