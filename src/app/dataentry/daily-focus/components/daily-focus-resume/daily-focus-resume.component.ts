import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PaginationInstance } from 'ngx-pagination';
import { ToastrService } from 'ngx-toastr';
import * as moment from 'moment';
import { IDailyFocus } from '../../interfaces/i-daily-focus';
import { DailyFocusService } from '../../services/daily-focus.service';
import { CatalogDataentryService } from '../../../shared/services/catalog-dataentry.service';
import { ResponseCatalogAircraftData } from '../../../shared/implements/response-catalog-aircraft-data';
import { ResponseCatalogData } from '../../../shared/implements/response-catalog-data';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-daily-focus-resumen',
  templateUrl: './daily-focus-resume.component.html',
  styleUrls: ['./daily-focus-resume.component.scss']
})
export class DailyFocusResumeComponent implements OnInit {

  public TITLE = 'Daily Focus';
  public records: IDailyFocus[];
  public typeAction: number;
  public titleModal: string;
  public modalRef: BsModalRef;
  // public valueSearch: string;
  public dailyForm: FormGroup;
  public dailyEdit: IDailyFocus;
  public recordDelete: string;
  public companies: ResponseCatalogData[];
  public customers: ResponseCatalogData[];
  public catalogReason: ResponseCatalogData[];
  public aeroplanes: ResponseCatalogAircraftData[];

  public configPage: PaginationInstance = {
    id: 'advanced',
    itemsPerPage: 10,
    currentPage: 1
  };

  constructor(
    private fb: FormBuilder,
    private dailyFocusService: DailyFocusService,
    private _notification: ToastrService,
    private _catalogService: CatalogDataentryService,
    private modalService: BsModalService,
    private _spinner: NgxSpinnerService,
  ) {
    this.companies = [];
    this.customers = [];
    this.catalogReason = [];
    this.aeroplanes = [];
    this.records = [];
  }

  ngOnInit() {
    this.initForm();
    this.getAllRecords();
  }

  spinnerHide() {
    return new Promise(resolve => {
      setTimeout(() => {
        this._spinner.hide().then(() => {
          resolve();
        });
      }, 1000);
    });
  }

  public onPageChange(number: number) {
    this.configPage.currentPage = number;
  }

  public getCompanies() {
    this._catalogService.getCompanies().subscribe(data => {
      this.companies = data;
    });
  }

  public getCustomers() {
    const company = this.dailyForm.controls.company.value;
    this._catalogService.getCustomers(company).subscribe(data => {
      this.customers = data;
      if (this.typeAction === 1) {
        this.setWorkOrder();
      }
    });
  }

  public setWorkOrder() {
    const aircraft = this.dailyForm.controls.aircraft.value;
    const aircraftSelected = this.findAircraftSelected(aircraft);
    this.dailyForm.controls
      .workOrder
      .setValue(`${aircraftSelected[0].woNumber}-${aircraftSelected[0].woCorr}-${aircraftSelected[0].woItem}`);
  }

  public getAircraft() {
    const company = this.dailyForm.controls.company.value;
    const customer = this.dailyForm.controls.customer.value;
    this._catalogService.getAircraft(company, customer).subscribe(data => {
      this.aeroplanes = data;
      this.dailyForm.controls.aircraft.setValue(data[0].aircraft);
      setTimeout(() => {
        this.setWorkOrder();
      }, 500);
    });
  }

  public getCatalogReason() {
    this._catalogService.getCatalog('FORM_DAILY_FOCUS_REASON').subscribe(data => {
      this.catalogReason = data;
    });
  }

  public getAllRecords() {
    this._spinner.show().then(() => {
      this.dailyFocusService.listAll().subscribe(resp => {
        this.spinnerHide().then(() => {
          if (resp.header.code === 200) {
            this.records = resp.data;
            this._notification.success('Action Complete', this.TITLE);
            if (this.records.length === 0) {
              this._notification.info('Record not found', this.TITLE);
            }
          } else {
            this._notification.error('Could not load data.', this.TITLE);
          }
        });
      }, error => {
        this.spinnerHide().then(() => {
          this._notification.error('Could not load data.', this.TITLE);
        });
      });
    });
  }

  public openModal(type, template, dai) {

    this._spinner.show().then(() => {
      this.typeAction = type;

      this.companies = [];
      this.customers = [];
      this.catalogReason = [];
      this.aeroplanes = [];

      this.dailyForm.reset();

      this.getCompanies();
      this.getCatalogReason();

      switch (type) {
        case 1 :
          this.titleModal = 'Daily Focus - Add';
          break;
        case 2 :
          this.dailyEdit = dai;
          this.titleModal = 'Daily Focus - Edit';
          this.dailyForm.controls.company.setValue('Default');
          this.dailyForm.controls.customer.setValue('Default');
          this.dailyForm.controls.aircraft.setValue(this.dailyEdit.aircraft);
          this.dailyForm.controls.workOrder.setValue(this.dailyEdit.workOrder);
          this.dailyForm.controls.task.setValue(this.dailyEdit.task);
          this.dailyForm.controls.complete.setValue(this.dailyEdit.complete);
          this.dailyForm.controls.reason.setValue(this.dailyEdit.reason);
          this.dailyForm.controls.dateId.setValue(moment(this.dailyEdit.dateId, 'YYYY-MM-DD').format('DD-MM-YYYY'));
          this.dailyForm.controls.dateId.setValue(moment.utc(this.dailyEdit.dateId).format('YYYY-MM-DD'));
          setTimeout(() => {
            this.getCustomers();
            this.getAircraft();
          }, 500);

          break;
      }

      this.spinnerHide().then(() => {
        this.modalRef = this.modalService.show(template, {
          class: 'modal-danger modal-lg',
          backdrop: true,
          ignoreBackdropClick: true,
          keyboard: false
        });
      });


    });


  }

  public submitForm() {
    switch (this.typeAction) {
      case 1:
        this.addRecord();
        break;
      case 2:
        this.editRecord();
        break;
    }
  }

  public addRecord() {
    console.log(this.dailyForm.valid);
    if (this.dailyForm.valid) {
      const body = {
        // company: this.dailyForm.getRawValue().company,
        // customer: this.dailyForm.getRawValue().customer,
        aircraft: this.dailyForm.getRawValue().aircraft,
        workOrder: this.dailyForm.getRawValue().workOrder,
        task: this.dailyForm.getRawValue().task,
        complete: this.dailyForm.getRawValue().complete,
        reason: this.dailyForm.getRawValue().reason,
        dateId: this.dailyForm.getRawValue().dateId,
      };
      this.dailyFocusService.create(body).subscribe(resp => {
        if (resp.header.code === 200) {
          this.modalRef.hide();
          this.getAllRecords();
          this.dailyForm.reset();
          this._notification.success('Daily Creado con Éxito', this.TITLE);
        } else {
          this._notification.error(resp.header.description, this.TITLE);
        }
      });
    } else {
      this._notification.warning('Complete toda la información requerida', this.TITLE);
    }
  }

  public editRecord() {
    if (this.dailyForm.valid) {
      const body = {
        id: this.dailyEdit.id,
        // company: this.dailyForm.getRawValue().company,
        // customer: this.dailyForm.getRawValue().customer,
        // aircraft: this.dailyForm.getRawValue().aircraft,
        workOrder: this.dailyForm.getRawValue().workOrder,
        task: this.dailyForm.getRawValue().task,
        complete: this.dailyForm.getRawValue().complete,
        reason: this.dailyForm.getRawValue().reason,
        dateId: this.dailyForm.getRawValue().dateId,
      };

      this.dailyFocusService.update(this.dailyEdit.id, body).subscribe(resp => {
        if (resp.header.code === 200) {
          this.modalRef.hide();
          this.getAllRecords();
          this._notification.success('Daily actualizado con Éxito', this.TITLE);
          this.dailyForm.reset();
        } else {
          this._notification.error(resp.header.description, this.TITLE);
        }
      });
    } else {
      this._notification.warning('Complete todos los campos', this.TITLE);
    }
  }

  public confirmation(id, template) {
    this.recordDelete = id;
    this.modalRef = this.modalService.show(template, {
      class: 'modal-primary',
      backdrop: true,
      ignoreBackdropClick: true,
      keyboard: false
    });
  }

  public delete() {
    this.dailyFocusService.delete(this.recordDelete).subscribe(resp => {
      if (resp.header.code === 200) {
        this.modalRef.hide();
        this.getAllRecords();
        this._notification.success('Daily Focus eliminado con Éxito', this.TITLE);
      } else {
        this._notification.error(resp.header.description, this.TITLE);
      }
    });
  }

  public closeModal() {
    this.modalRef.hide();
    this.dailyForm.reset();
  }

  private findAircraftSelected(aircraft: string): ResponseCatalogAircraftData[] {
    return this.aeroplanes.filter(item => {
      return item.aircraft === aircraft;
    });
  }

  private initForm() {
    this.dailyForm = this.fb.group({
      // company: ['', Validators.compose([Validators.required])],
      company: [''],
      // customer: ['', Validators.compose([Validators.required])],
      customer: [''],
      // aircraft: ['', Validators.compose([Validators.required])],
      aircraft: [''],
      workOrder: ['', Validators.compose([Validators.required])],
      task: ['', Validators.compose([Validators.required])],
      complete: [''],
      reason: [''],
      dateId: ['', Validators.compose([Validators.required])],
    });
  }

}
