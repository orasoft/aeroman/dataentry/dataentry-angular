import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyFocusResumeComponent } from './daily-focus-resume.component';

describe('DailyFocusResumenComponent', () => {
  let component: DailyFocusResumeComponent;
  let fixture: ComponentFixture<DailyFocusResumeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DailyFocusResumeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DailyFocusResumeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
