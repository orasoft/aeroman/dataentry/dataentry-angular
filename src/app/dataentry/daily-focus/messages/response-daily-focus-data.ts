import { ResponseGenericData } from '../../../shared/responses/response-generic-data';
import { IHeader } from '../../../shared/interfaces/i-header';
import { IDailyFocus } from '../interfaces/i-daily-focus';

export class ResponseDailyFocusData implements ResponseGenericData<IDailyFocus[]> {
  data: IDailyFocus[];
  header: IHeader;
}
