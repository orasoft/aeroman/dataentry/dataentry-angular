export interface IDailyFocus {
  id?: string;
  company: string;
  customer: string;
  aircraft: string;
  workOrder: string;
  task: string;
  complete: number;
  reason: string;
  dateId: Date;
  mrohSource: string;
}
