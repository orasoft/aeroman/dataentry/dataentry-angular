import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { DailyFocusResumeComponent } from './components/daily-focus-resume/daily-focus-resume.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DailyFocusService } from './services/daily-focus.service';
import { HttpClientModule } from '@angular/common/http';
import { SharedDataentryModule } from '../shared/shared-dataentry.module';
import { NgxMaskModule } from 'ngx-mask';

const routes: Routes = [
  {
    path: 'resume',
    component: DailyFocusResumeComponent,
    data: {
      title: 'Daily Focus'
    }
  }
];

@NgModule({
  declarations: [DailyFocusResumeComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    NgxPaginationModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    SharedDataentryModule,
    NgxMaskModule,
  ],
  providers: [
    DailyFocusService
  ]
})
export class DailyFocusModule {
}
