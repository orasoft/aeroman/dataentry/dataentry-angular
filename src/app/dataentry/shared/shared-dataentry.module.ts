import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CatalogDataentryService } from './services/catalog-dataentry.service';
import { FormsModule } from '@angular/forms';
import { CatalogDatahubService } from './services/catalog-datahub.service';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    FormsModule
  ],
  providers: [
    CatalogDataentryService,
    CatalogDatahubService
  ],
  exports: []
})
export class SharedDataentryModule {
}
