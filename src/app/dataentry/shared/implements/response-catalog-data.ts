export interface ResponseCatalogData {
  label: string;
  value: string;
}
