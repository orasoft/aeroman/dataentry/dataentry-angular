export interface ResponseCatalogAircraftData {
  aircraft: string;
  woNumber: number;
  woCorr: number;
  woItem: number;
}
