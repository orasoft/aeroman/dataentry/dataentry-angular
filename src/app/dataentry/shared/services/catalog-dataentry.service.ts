import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ResponseCatalogData } from '../implements/response-catalog-data';
import { Observable } from 'rxjs';
import { ResponseCatalogAircraftData } from '../implements/response-catalog-aircraft-data';
import { environment } from '../../../../environments/environment';

@Injectable()
export class CatalogDataentryService {

  private _url = environment.apiUrl + '/dataentry/catalog';

  constructor(private readonly _http: HttpClient) {
  }

  getCompanies(): Observable<ResponseCatalogData[]> {
    return this._http.get<ResponseCatalogData[]>(`${this._url}/company`);
  }

  getCustomers(company: string): Observable<ResponseCatalogData[]> {
    return this._http.get<ResponseCatalogData[]>(`${this._url}/customer/${company}`);
  }

  getAircraft(company: string, customer: string): Observable<ResponseCatalogAircraftData[]> {
    return this._http.get<ResponseCatalogAircraftData[]>(`${this._url}/aircraft/${company}/${customer}`);
  }

  getCatalog(id: string): Observable<ResponseCatalogData[]> {
    return this._http.get<ResponseCatalogData[]>(`${this._url}/items/${id}`);
  }

}
