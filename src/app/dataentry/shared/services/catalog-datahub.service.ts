import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ResponseCompanyData } from '../../../datahub/shared/response/response-company-data';
import { ResponseTableInData } from '../../../datahub/shared/response/response-table-in-data';
import { ResponseFieldsInData } from '../../../datahub/shared/response/response-fields-in-data';
import { ResponseModelsData } from '../../../datahub/shared/response/response-models-data';
import { ResponseTableOutData } from '../../../datahub/shared/response/response-table-out-data';
import { ResponseFieldsOutData } from '../../../datahub/shared/response/response-fields-out-data';

// TODO - Eliminar
@Injectable()
export class CatalogDatahubService {
  private _url = environment.apiUrl + '/datahub';

  constructor(private readonly _http: HttpClient) {
  }

  getCompanies(): Observable<ResponseCompanyData> {
    return this._http.get<ResponseCompanyData>(`${this._url}/company`);
  }

  getTableIn(): Observable<ResponseTableInData> {
    return this._http.get<ResponseTableInData>(`${this._url}/table_in`);
  }

  getFieldsIn(): Observable<ResponseFieldsInData> {
    return this._http.get<ResponseFieldsInData>(`${this._url}/fields_in`);
  }

  getModels(): Observable<ResponseModelsData> {
    return this._http.get<ResponseModelsData>(`${this._url}/models`);
  }

  getTablesOut(): Observable<ResponseTableOutData> {
    return this._http.get<ResponseTableOutData>(`${this._url}/table_out`);
  }

  getFieldsOut(): Observable<ResponseFieldsOutData> {
    return this._http.get<ResponseFieldsOutData>(`${this._url}/fields_out`);
  }
}
