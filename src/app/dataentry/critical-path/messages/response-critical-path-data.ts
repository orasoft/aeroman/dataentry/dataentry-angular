import { ResponseGenericData } from '../../../shared/responses/response-generic-data';
import { ICriticalPath } from '../interfaces/i-critical-path';
import { IHeader } from '../../../shared/interfaces/i-header';

export class ResponseCriticalPathData implements ResponseGenericData<ICriticalPath[]> {
  data: ICriticalPath[];
  header: IHeader;
}
