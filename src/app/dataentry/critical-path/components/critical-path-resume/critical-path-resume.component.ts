import { Component, OnInit } from '@angular/core';
import { ICriticalPath } from '../../interfaces/i-critical-path';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PaginationInstance } from 'ngx-pagination';
import { ToastrService } from 'ngx-toastr';
import { CriticalPathService } from '../../services/critical-path.service';
import { ResponseCatalogData } from '../../../shared/implements/response-catalog-data';
import { CatalogDataentryService } from '../../../shared/services/catalog-dataentry.service';
import { ResponseCatalogAircraftData } from '../../../shared/implements/response-catalog-aircraft-data';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-critical-path-resumen',
  templateUrl: './critical-path-resume.component.html',
  styleUrls: ['./critical-path-resume.component.scss']
})
export class CriticalPathResumeComponent implements OnInit {

  public TITLE = 'Critical Paths Items';
  public records: ICriticalPath[];
  public typeAction: number;
  public titleModal: string;
  public modalRef: BsModalRef;
  // public valueSearch: string;
  public criticalForm: FormGroup;
  public criticalEdit: ICriticalPath;
  public recordDelete: string;
  public companies: ResponseCatalogData[];
  public customers: ResponseCatalogData[];
  public catalogResponsible: ResponseCatalogData[];
  public catalogStatus: ResponseCatalogData[];
  public aeroplanes: ResponseCatalogAircraftData[];

  public configPage: PaginationInstance = {
    id: 'advanced',
    itemsPerPage: 10,
    currentPage: 1
  };

  constructor(
    private _criticalPathService: CriticalPathService,
    private _modalService: BsModalService,
    private _fb: FormBuilder,
    private _catalogService: CatalogDataentryService,
    private _notification: ToastrService,
    private _spinner: NgxSpinnerService,
  ) {
    this.companies = [];
    this.customers = [];
    this.aeroplanes = [];
    this.records = [];
  }

  ngOnInit() {
    this.initForm();
    this.getAllRecords();
  }

  spinnerHide() {
    return new Promise(resolve => {
      setTimeout(() => {
        this._spinner.hide().then(() => {
          resolve();
        });
      }, 1000);
    });
  }

  public onPageChange(number: number) {
    this.configPage.currentPage = number;
  }

  public getAllRecords() {
    this._spinner.show().then(() => {
      this._criticalPathService.listCritical().subscribe(resp => {
        this.spinnerHide().then(() => {
          if (resp.header.code === 200) {
            this.records = resp.data;
            this._notification.success('Action Complete', this.TITLE);
            if (this.records.length === 0) {
              this._notification.info('Record not found', this.TITLE);
            }
          } else {
            this._notification.error('Could not load data.', this.TITLE);
          }
        });
      }, error => {
        this.spinnerHide().then(() => {
          this._notification.error('Could not load data.', this.TITLE);
        });
      });
    });
  }

  public openModal(type, template, crit?) {

    this._spinner.show().then(() => {

      this.typeAction = type;
      this.companies = [];
      this.customers = [];
      this.aeroplanes = [];
      this.catalogResponsible = [];

      this.criticalForm.reset();

      this.getCompanies();
      this.getCatalogResponsible();
      this.getCatalogStatus();

      switch (type) {
        case 1 :
          this.titleModal = 'Critical Paths - Add';
          break;
        case 2 :
          this.criticalEdit = crit;
          this.titleModal = 'Critical Paths - Edit';
          this.criticalForm.controls.company.setValue('Default');
          this.criticalForm.controls.customer.setValue('Default');
          this.criticalForm.controls.aircraft.setValue(this.criticalEdit.aircraft);
          this.criticalForm.controls.workOrder.setValue(this.criticalEdit.workOrder);
          this.criticalForm.controls.criticalPath.setValue(this.criticalEdit.criticalPath);
          this.criticalForm.controls.tatImpact.setValue(this.criticalEdit.tatImpact);
          this.criticalForm.controls.mitigationPlan.setValue(this.criticalEdit.mitigationPlan);
          this.criticalForm.controls.responsible.setValue(this.criticalEdit.responsible);
          this.criticalForm.controls.status.setValue(this.criticalEdit.status);

          setTimeout(() => {
            this.getCustomers();
            this.getAircraft();
          }, 500);
          break;
      }

      this.spinnerHide().then(() => {
        this.modalRef = this._modalService.show(template, {
          class: 'modal-danger modal-lg',
          backdrop: true,
          ignoreBackdropClick: true,
          keyboard: false
        });
      });
    });


  }

  public submitForm() {
    switch (this.typeAction) {
      case 1:
        this.addRecord();
        break;
      case 2:
        this.editRecord();
        break;
    }
  }

  public addRecord() {
    if (this.criticalForm.valid) {
      const body = {
        company: this.criticalForm.getRawValue().company,
        customer: this.criticalForm.getRawValue().customer,
        aircraft: this.criticalForm.getRawValue().aircraft,
        workOrder: this.criticalForm.getRawValue().workOrder,
        criticalPath: this.criticalForm.getRawValue().criticalPath,
        tatImpact: this.criticalForm.getRawValue().tatImpact,
        mitigationPlan: this.criticalForm.getRawValue().mitigationPlan,
        responsible: this.criticalForm.getRawValue().responsible,
        status: this.criticalForm.getRawValue().status,
        statusFinal: this.criticalForm.getRawValue().status,
      };

      this._spinner.show().then(() => {
        this._criticalPathService.createCritical(body).subscribe(resp => {
          if (resp.header.code === 200) {
            this.modalRef.hide();
            this._spinner.hide();
            this.getAllRecords();
            this._notification.success('Record saved.', this.TITLE);
            this.criticalForm.reset();
          } else {
            this._spinner.hide();
            this._notification.error(resp.header.description, this.TITLE);
          }
        }, error => {
          this.spinnerHide().then(() => {
            this._notification.error('It was not possible to save the record.', this.TITLE);
          });
        });
      });
    } else {
      this._notification.warning('Verify the information, some fields are required.', this.TITLE);
    }
  }

  public editRecord() {
    if (this.criticalForm.valid) {
      const body = {
        id: this.criticalEdit.id,
        company: this.criticalForm.getRawValue().company,
        customer: this.criticalForm.getRawValue().customer,
        aircraft: this.criticalForm.getRawValue().aircraft,
        workOrder: this.criticalForm.getRawValue().workOrder,
        criticalPath: this.criticalForm.getRawValue().criticalPath,
        tatImpact: this.criticalForm.getRawValue().tatImpact,
        mitigationPlan: this.criticalForm.getRawValue().mitigationPlan,
        responsible: this.criticalForm.getRawValue().responsible,
        status: this.criticalForm.getRawValue().status,
        statusFinal: this.criticalForm.getRawValue().status,
      };

      this._criticalPathService.updateCritical(this.criticalEdit.id, body).subscribe(resp => {
        if (resp.header.code === 200) {
          this.modalRef.hide();
          this.getAllRecords();
          this._notification.success('Critical Paths actualizado con Éxito', this.TITLE);
          this.criticalForm.reset();
        } else {
          this._notification.error(resp.header.description, this.TITLE);
        }
      });
    } else {
      this._notification.warning('Complete todos los campos', this.TITLE);
    }
  }

  public confirmation(id, template) {
    this.recordDelete = id;
    this.modalRef = this._modalService.show(template, {
      class: 'modal-primary',
      backdrop: true,
      ignoreBackdropClick: true,
      keyboard: false
    });
  }

  public delete() {
    this._criticalPathService.deleteCritical(this.recordDelete).subscribe(resp => {
      if (resp.header.code === 200) {

        this.modalRef.hide();
        this.getAllRecords();
        this._notification.success('Critical Paths eliminado con Éxito', this.TITLE);
      } else {
        this._notification.error(resp.header.description, this.TITLE);
      }
    });
  }

  public closeModal() {
    this.modalRef.hide();
    this.criticalForm.reset();
  }

  public getCompanies() {
    this._catalogService.getCompanies().subscribe(data => {
      this.companies = data;
    });
  }

  public getCustomers() {
    const company = this.criticalForm.controls.company.value;
    this._catalogService.getCustomers(company).subscribe(data => {
      this.customers = data;
      if (this.typeAction === 1) {
        this.setWorkOrder();
      }
    });
  }

  public getAircraft() {
    const company = this.criticalForm.controls.company.value;
    const customer = this.criticalForm.controls.customer.value;
    this._catalogService.getAircraft(company, customer).subscribe(data => {
      this.aeroplanes = data;
      this.criticalForm.controls.aircraft.setValue(data[0].aircraft);
      setTimeout(() => {
        this.setWorkOrder();
      }, 500);
    });
  }

  public getCatalogResponsible() {
    this._catalogService.getCatalog('FORM_CRITICAL_PATHS_RESPONSIBLE').subscribe(data => {
      this.catalogResponsible = data;
    });
  }

  public getCatalogStatus() {
    this._catalogService.getCatalog('FORM_CRITICAL_PATHS_STATUS').subscribe(data => {
      this.catalogStatus = data;
    });
  }

  public setWorkOrder() {
    const aircraft = this.criticalForm.controls.aircraft.value;
    const aircraftSelected = this.findAircraftSelected(aircraft);
    this.criticalForm.controls.workOrder.setValue(`${aircraftSelected[0].woNumber} ${aircraftSelected[0].woCorr}/${aircraftSelected[0].woItem}`);
  }

  private findAircraftSelected(aircraft: string): ResponseCatalogAircraftData[] {
    return this.aeroplanes.filter(item => {
      return item.aircraft === aircraft;
    });
  }

  private initForm() {
    this.criticalForm = this._fb.group({
      company: ['', Validators.compose([Validators.required])],
      customer: ['', Validators.compose([Validators.required])],
      aircraft: ['', Validators.compose([Validators.required])],
      workOrder: ['', Validators.compose([Validators.required])],
      criticalPath: ['', Validators.compose([Validators.required])],
      tatImpact: [0, Validators.compose([Validators.required, Validators.min(0)])],
      mitigationPlan: ['', Validators.compose([Validators.required])],
      responsible: ['', Validators.compose([Validators.required])],
      status: ['', Validators.compose([Validators.required])]
    });
  }

}
