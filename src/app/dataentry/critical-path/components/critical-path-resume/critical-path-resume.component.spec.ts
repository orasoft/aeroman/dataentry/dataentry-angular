import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CriticalPathResumeComponent } from './critical-path-resume.component';

describe('CriticalPathResumenComponent', () => {
  let component: CriticalPathResumeComponent;
  let fixture: ComponentFixture<CriticalPathResumeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CriticalPathResumeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CriticalPathResumeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
