import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { CriticalPathResumeComponent } from './components/critical-path-resume/critical-path-resume.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CriticalPathService } from './services/critical-path.service';
import { HttpClientModule } from '@angular/common/http';
import { SharedDataentryModule } from '../shared/shared-dataentry.module';
import { NgxMaskModule } from 'ngx-mask';

const routes: Routes = [
  {
    path: 'resume',
    component: CriticalPathResumeComponent,
    data: {
      title: 'Critical Paths'
    }
  }
];

@NgModule({
  declarations: [
    CriticalPathResumeComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    NgxPaginationModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    SharedDataentryModule,
    NgxMaskModule,
  ],
  providers: [
    CriticalPathService
  ]
})
export class CriticalPathModule {
}
