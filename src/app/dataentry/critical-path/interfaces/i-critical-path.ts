export interface ICriticalPath {
  id?: string;
  company: string;
  customer: string;
  aircraft: string;
  workOrder: string;
  criticalPath: string;
  tatImpact: number;
  mitigationPlan: string;
  responsible: string;
  status: string;
  statusFinal: string;
}
