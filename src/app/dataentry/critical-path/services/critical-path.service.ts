import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ResponseCriticalPathData } from '../messages/response-critical-path-data';
import { environment } from '../../../../environments/environment';

@Injectable()
export class CriticalPathService {

  private _url = environment.apiUrl + '/dataentry';

  constructor(private readonly _http: HttpClient) {
  }

  public listCritical(): Observable<ResponseCriticalPathData> {
    return this._http.get<ResponseCriticalPathData>(`${this._url}/critical_paths`);
  }

  public createCritical(body): Observable<ResponseCriticalPathData> {
    return this._http.post<ResponseCriticalPathData>(`${this._url}/critical_paths`, body);
  }

  public updateCritical(id: string, body): Observable<ResponseCriticalPathData> {
    return this._http.put<ResponseCriticalPathData>(`${this._url}/critical_paths/${id}`, body);
  }

  public deleteCritical(id): Observable<ResponseCriticalPathData> {
    return this._http.delete<ResponseCriticalPathData>(`${this._url}/critical_paths/${id}`);
  }

  public findCritical(value: string): Observable<ResponseCriticalPathData> {
    return this._http.get<ResponseCriticalPathData>(`${this._url}/critical_paths/find/${value}`);
  }
}
