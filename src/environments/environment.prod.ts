export const environment = {
  production: true,
  environmentName: 'prod',
  version: '1.0.8',
  apiUrl: 'http://172.23.123.243:8150/aeroman/api/v1'
};
